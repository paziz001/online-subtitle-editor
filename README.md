#**An Online Subtitle Editor for the Matterhorn System:**
***
###**Features:**
- Create a new .srt format subtitle file.
- Add existing .srt file and edit it.
- Download the edited .srt file.
- Insert video for assistance in syncing the subtitle correctly.
- A Table with the subtitles and operations for deleting a subtitle, adding a new subtitle, inserting a subtitle before or a after a selected subtitle and editing a selected subtitle from the table.
- Auto break subtitle feature.

