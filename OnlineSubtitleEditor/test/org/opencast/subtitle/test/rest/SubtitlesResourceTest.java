package org.opencast.subtitle.test.rest;


import static org.junit.Assert.assertTrue;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import javax.json.Json;
import javax.json.JsonArray;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SubtitlesResourceTest {
    
	String uri;
	Client client;
    
    WebTarget webTarget;
    
    @Before
	public void setUp() throws Exception {
		this.uri="http://localhost:8080/OnlineSubtitleEditor/rest/subtitleService";
		this.client=ClientBuilder.newClient(new ClientConfig());
		this.webTarget = this.client.target(uri);
	}
	
	@Test
	public void testDownloadSubtitleFile() {
		WebTarget resourceWebTarget= webTarget.path("/downloadSubtitleFile");
		Invocation.Builder invocationBuilder =
	            resourceWebTarget.request("text/srt");
	    invocationBuilder.header("accept", "true");
	    Response response = invocationBuilder.get();
	    System.out.println("testDownloadSubtitlesJson():");
	    System.out.println(resourceWebTarget.toString());
	    System.out.println("Status: "+response.getStatus()+" "+response.getStatusInfo());
	    System.out.println("Content:\n"+response.readEntity(String.class)+"\n");
	    assertTrue("Expected response status Bad Request or OK",response.getStatusInfo().toString().equals("Bad Request")
	    		||response.getStatusInfo().toString().equals("OK"));
	}

	@Test
	public void testAddExistingSubtitleFile() {
		InputStream is;
		try {
			is = new FileInputStream(System.getProperty("user.dir")+"/SampleSubtitle/google-dev.srt");
			WebTarget resourceWebTarget= webTarget.path("/addExistingSubtitleFile");
			Invocation.Builder invocationBuilder =
		            resourceWebTarget.request("text/srt");
		    invocationBuilder.header("accept", "true");
		    Response response = invocationBuilder.post(Entity.entity(is, MediaType.MULTIPART_FORM_DATA));
		    System.out.println("testAddExistingSubtitleFile():");
		    System.out.println(resourceWebTarget.toString());
		    System.out.println("Status: "+response.getStatus()+" "+response.getStatusInfo()+"\n");
		    assertTrue("Expected response status Bad Request or OK",response.getStatusInfo().toString().equals("Bad Request")
		    		||response.getStatusInfo().toString().equals("OK"));
		} catch (FileNotFoundException e) {
		
			e.printStackTrace();
		}
		
		
	}

	@Test
	public void testInsertSubtitlesJson() {
		WebTarget resourceWebTarget= webTarget.path("/insertSubtitles");
		JsonArray value = Json.createArrayBuilder().add(Json.createObjectBuilder()
				.add("starttime","00:00:00.000")
				.add("endtime","00:00:00.000")
				.add("content","First Content"))
				.add(Json.createObjectBuilder()
						.add("starttime","00:00:00.000")
						.add("endtime","00:00:00.000")
						.add("content","Second Content")).build();
		Invocation.Builder invocationBuilder =
	            resourceWebTarget.request("application/json");
	    invocationBuilder.header("accept", "true");
	    Response response = invocationBuilder.post(Entity.entity(value.toString(), MediaType.APPLICATION_JSON));
	    System.out.println("testInsertSubtitlesJson():");
	    System.out.println(resourceWebTarget.toString());
	    System.out.println("Status: "+response.getStatus()+" "+response.getStatusInfo()+"\n");
	    assertTrue("Expected response status Bad Request or OK",response.getStatusInfo().toString().equals("Bad Request")
	    		||response.getStatusInfo().toString().equals("OK"));
	}

	@Test
	public void testGetSubtitlesJson() {
		WebTarget resourceWebTarget= webTarget.path("/getSubtitles");
		Invocation.Builder invocationBuilder =
	            resourceWebTarget.request("application/json");
	    invocationBuilder.header("accept", "true");
	    Response response = invocationBuilder.get();
	    System.out.println("testGetSubtitlesJson():");
	    System.out.println(resourceWebTarget.toString());
	    System.out.println("Status: "+response.getStatus()+" "+response.getStatusInfo());
	    System.out.println("Content:\n"+response.readEntity(String.class)+"\n");
	    assertTrue("Expected response status Bad Request or OK",response.getStatusInfo().toString().equals("Bad Request")
	    		||response.getStatusInfo().toString().equals("OK"));
	}
	
	@Test
	public void testEmptySubtitleDao() {
		WebTarget resourceWebTarget= webTarget.path("/emptySubtitleDao");
		Invocation.Builder invocationBuilder =
	            resourceWebTarget.request();
	    invocationBuilder.header("accept", "true");
	    Response response = invocationBuilder.delete();
	    System.out.println("testemptySubtitleDao():");
	    System.out.println(resourceWebTarget.toString());
	    System.out.println("Status: "+response.getStatus()+" "+response.getStatusInfo()+"\n");
	    assertTrue("Expected response status Bad Request or OK",response.getStatusInfo().toString().equals("Bad Request")
	    		||response.getStatusInfo().toString().equals("OK"));
	}

}
