package org.opencast.subtitle.test.rest;


import static org.junit.Assert.*;

import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;
import org.junit.Before;
import org.junit.Test;

public class SubtitleResourceTest {
	String uri;
	Client client;
    
    WebTarget webTarget;
	@Before
	public void setUp() throws Exception {
		this.uri="http://localhost:8080/OnlineSubtitleEditor/rest/subtitleService";
		this.client=ClientBuilder.newClient(new ClientConfig());
		this.webTarget = this.client.target(uri);
	}

	@Test
	public void testGetSubtitle() {
		WebTarget resourceWebTarget= webTarget.path("/subtitle/1");
		JsonObject value = Json.createObjectBuilder()
				.add("starttime","00:00:00.000")
				.add("endtime","00:00:00.000")
				.add("content","Sample Content").build();
	    Invocation.Builder invocationBuilder =
	            resourceWebTarget.request("application/json");
	    invocationBuilder.header("accept", "true");
	    Invocation.Builder invocationBuilder2 =
	            resourceWebTarget.request("application/json");
	    invocationBuilder.header("accept", "true");
	    Response response = invocationBuilder2
	    		.post(Entity
	    			.entity(value.toString(), MediaType.APPLICATION_JSON));
	    response = invocationBuilder.get();
	    System.out.println("testGetSubtitle():");
	    System.out.println(resourceWebTarget.toString());
	    System.out.println("Status: "+response
	    		.getStatus()+" "+response.getStatusInfo()+"\n");
	    System.out.println("Content:\n"+response.readEntity(String.class)+"\n");
	    assertTrue("Expected response status Not Found or OK",response.getStatusInfo().toString().equals("Not Found")
	    		||response.getStatusInfo().toString().equals("OK"));
	    
	}
	@Test
	public void testPostSubtitle() {
		WebTarget resourceWebTarget= webTarget.path("/subtitle/1");
		JsonObject value = Json.createObjectBuilder()
				.add("starttime","00:00:00.000")
				.add("endtime","00:00:00.000")
				.add("content","Posted Content").build();
	    Invocation.Builder invocationBuilder =
	            resourceWebTarget.request("application/json");
	    invocationBuilder.header("accept", "true");
	    Response response = invocationBuilder.post(Entity.entity(value.toString(), MediaType.APPLICATION_JSON));
	    System.out.println("testPostSubtitle():");
	    System.out.println(resourceWebTarget.toString());
	    System.out.println("Status: "+response.getStatus()+" "+response.getStatusInfo().toString()+"\n");
	    System.out.println("Content:\n"+response.readEntity(String.class)+"\n");
	    assertTrue("Expected response status Bad Request or OK",response.getStatusInfo().toString().equals("Bad Request")
	    		||response.getStatusInfo().toString().equals("OK"));
	    
	}

	@Test
	public void testPutSubtitle() {
		WebTarget resourceWebTarget= webTarget.path("/subtitle/1");
		JsonObject value = Json.createObjectBuilder()
				.add("starttime","00:00:00.000")
				.add("endtime","00:00:00.000")
				.add("content","Edited Posted Content").build();
	    Invocation.Builder invocationBuilder =
	            resourceWebTarget.request("application/json");
	    invocationBuilder.header("accept", "true");
	    
	    Response response = invocationBuilder.put(Entity.entity(value.toString(), MediaType.APPLICATION_JSON));
	    System.out.println("testPutSubtitle():");
	    System.out.println(resourceWebTarget.toString());
	    System.out.println("Status: "+response.getStatus()+" "+response.getStatusInfo()+"\n");
	    System.out.println("Content:\n"+response.readEntity(String.class)+"\n");
	    assertTrue("Expected response status Bad Request or OK",response.getStatusInfo().toString().equals("Bad Request")
	    		||response.getStatusInfo().toString().equals("OK"));
	}

	@Test
	public void testDeleteSubtitle() {
		WebTarget resourceWebTarget= webTarget.path("/subtitle/1");
	    Invocation.Builder invocationBuilder =
	            resourceWebTarget.request("application/json");
	    invocationBuilder.header("accept", "true");
	    
	    Response response = invocationBuilder.delete();
	    System.out.println("testDeleteSubtitle():");
	    System.out.println(resourceWebTarget.toString());
	    System.out.println("Status: "+response.getStatus()+" "+response.getStatusInfo()+"\n");
	    System.out.println("Content:\n"+response.readEntity(String.class)+"\n");
	    assertTrue("Expected response status Not Found or OK",response.getStatusInfo().toString().equals("Not Found")
	    		||response.getStatusInfo().toString().equals("OK"));
	}

}
