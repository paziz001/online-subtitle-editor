/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencast.subtitle.test.impl;



import org.opencastproject.subtitle.impl.SubtitleImpl;
import org.opencastproject.subtitle.impl.TimeImpl;

import static org.junit.Assert.*;

import org.junit.AfterClass;
import org.junit.Test;

/**
 * @author User
 *
 */
public class SubtitleImplTest {

  /**
   * @throws java.lang.Exception
   */
  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  /**
   * Test method for {@link org.opencastproject.subtitle.impl.SubtitleImpl#toString()}.
   */
  @Test
  public void testToString() {
    SubtitleImpl subtitle1 = new SubtitleImpl(new TimeImpl("2:1:1.1"),
    		new TimeImpl("2:1:1.1"),"Greetings\nPeace"
    		.split("[\0\n]"));
    System.out.println("02:01:01,001 --> 02:01:01,001\r\nGreetings\r\nPeace\r\n");
    System.out.println(subtitle1.toString());
    assertTrue("Must print 02:01:01,001 --> 02:01:01,001", 
    		"02:01:01,001 --> 02:01:01,001\r\nGreetings\r\nPeace\r\n"
    		.equals(subtitle1.toString()));
  }

}
