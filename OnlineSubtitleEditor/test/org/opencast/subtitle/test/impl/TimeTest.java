package org.opencast.subtitle.test.impl;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.opencastproject.subtitle.impl.TimeImpl;

/*
 * class for testing TestImpl's toString method.
 * 
 */
public class TimeTest {
  @Test
  public void testToString() {
    TimeImpl time1 = new TimeImpl("01:01:01.001");
    TimeImpl time2 = new TimeImpl("11:11:11.011");
    TimeImpl time3 = new TimeImpl("11:00:00.111");
    assertTrue("Time(1,1,1,1).toString must output 01:01:01.001"
    		,"01:01:01.001".equals(time1.toString()));
    assertTrue("Time(11,11,11,11).toString must output 11:11:11.011"
    		, "11:11:11.011".equals(time2.toString())); 
    assertTrue("Time(11,0,0,111).toString must output 11:00:00.111"
    		,"11:00:00.111".equals(time3.toString()));
    
  }

}
