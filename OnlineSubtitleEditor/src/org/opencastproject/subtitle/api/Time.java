package org.opencastproject.subtitle.api;

/**
 * Time representation of single subtitle.
 */
public interface Time {
  
  /**
   * Get hours of a subtitle.
   *
   * @return hours
   */
  int getHours();

  /**
   * Get minutes of a subtitle.
   *
   * @return minutes
   */
  int getMinutes();

  /**
   * Get seconds of a subtitle.
   *
   * @return seconds
   */
  int getSeconds();

  /**
   * Get milliseconds of a subtitle.
   *
   * @return milliseconds
   */
  int getMilliseconds();
  
  /**
   * Set hours. 
   * 
   */
  void setHours(int hours);
  
  /**
   * Set minutes
   * 
   */
   void setMinutes(int minutes);
   
   /**
    * Set seconds
    * 
    */
    void setSeconds(int seconds);
    
    /**
     * Set milliseconds 
     */
    void setMilliseconds(int milliseconds);
    
   /**
    * Display time in hh:mm:ss.mmm
    */
   String toString();
}


