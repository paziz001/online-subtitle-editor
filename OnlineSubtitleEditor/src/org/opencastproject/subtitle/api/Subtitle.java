package org.opencastproject.subtitle.api;

import org.opencastproject.subtitle.impl.TimeImpl;

/**
 * 
 * Subtitle creation and processing operations  
 *
 */

public interface Subtitle {
  
   
  /**
   * Get the start time of the subtitle.
   *
   * @return subtitle start time
   */
  
  String getStartTime();

  /**
   * Get the end time of the subtitle.
   *
   * @return subtitle end time
   */
  String getStopTime();

  /**
   * Get the subtitle text.
   *
   * @return subtitle text
   */
  String getSubtitle();
  
  /**
   * Set the start time of the subtitle.
   *
   * @return subtitle start time
   */
  void setStartTime(TimeImpl startTime);

  /**
   * Set the end time of the subtitle.
   *
   * @return subtitle end time
   */
  void setStopTime(TimeImpl stopTime);

  /**
   * Set the subtitle text.
   *
   * @return subtitle text
   */
  void setSubtitle(String[] subtitle);
  
  /**
   * Returns the subtitle in the format that it will
   * be displayed in the srt file. 
   * 
 * @return Subtitle representation 
 */
  String toString();
  
}
