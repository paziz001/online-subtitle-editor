package org.opencastproject.subtitle.rest;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import org.opencastproject.subtitle.errorhandling.ErrorMessage;
import org.opencastproject.subtitle.impl.SubtitleDao;
import org.opencastproject.subtitle.impl.SubtitleImpl;
import org.opencastproject.subtitle.impl.TimeImpl;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.xml.internal.fastinfoset.util.StringArray;

/**
 * @author Philippos Aziz
 *
 */
@Path("subtitleService/")
public class SubtitlesResource {
 	
 @Context
 UriInfo uriInfo;
 @Context
 Request request;
 
 /**
  * Sends the srt file to the client for saving .
  * 
 * @return status OK when successfull or BAD_REQUEST when an esception occurs
 */
 @Path("downloadSubtitleFile/")
 @GET
 @Produces("text/srt")
 public Response downloadSubtitleFile(){
   File subtitleFile;
   ResponseBuilder response;
  try {
    subtitleFile = File.createTempFile("subtitle", ".srt");
    FileOutputStream fout = new FileOutputStream(subtitleFile.getAbsoluteFile());
    Writer out = new OutputStreamWriter(fout, "UTF8");  

    for(int i=0;i<SubtitleDao.instance.getSubtitles().size();i++){
      out.write((i+1)+"\r\n"+SubtitleDao.instance.getSubtitles().get(i).toString());
    }
    out.close();
    response = Response.ok((Object) subtitleFile);
    response.header("Content-Disposition",
      "attachment; filename=newSubtitleFile.srt");
    subtitleFile.deleteOnExit();
  } catch (IOException e) {
	  return Response.status(Status.BAD_REQUEST)
				.entity(new ErrorMessage("error", "IOException"))
				.type(MediaType.APPLICATION_JSON).build();
  } 
  return response.status(Status.OK).build();
 }

/**
 * Clears the subtitle data structure
 * 
 * @return Response Status OK
 */
@Path("emptySubtitleDao/")
@DELETE
public Response emptySubtitleDao(){
	if(!SubtitleDao.instance.getSubtitles().isEmpty()){
		SubtitleDao.instance.getSubtitles().clear();
	   }
	return Response.ok().build();
}

 /**
  * Reads existing subtitles from the srt file 
  * and adds each one of them as an object in 
  * the subtitles data model.
 * @param file
 * @return Response with text/srt file
 */
 @Path("addExistingSubtitleFile/")
 @POST
 @Consumes(MediaType.MULTIPART_FORM_DATA)
 public Response addExistingSubtitleFile(InputStream file){
   if(!SubtitleDao.instance.getSubtitles().isEmpty()){
	  SubtitleDao.instance.getSubtitles().clear(); 
   }
   File subtitleFile;
   try {
     subtitleFile = File.createTempFile("subtitletmp", ".srt");
     BufferedReader br = new BufferedReader( new InputStreamReader(file, "UTF8"));
     Pattern time = Pattern.compile("[0-9]{2}:[0-9]{2}:[0-9]{2}");
     Pattern id = Pattern.compile("^[0-9]+$");
     Pattern content = Pattern.compile(".+");
     Pattern header = Pattern.compile("(^Content-Disposition:.+|"
             + "^Content-Type:.+|"
             + "^-{5,}[0-9]+|"
             + "^-{5,}WebKitFormBoundary[0-9a-zA-Z]+|"
             + "^$)");
     Pattern srt = Pattern.compile("^Content-Disposition:.+[.]{1}srt[^.]");
     int counter=0;
     StringArray subcontent=new StringArray();
     SubtitleImpl subtitle = new SubtitleImpl();
     for(String line; (line = br.readLine()) != null; ) {
       Matcher mhead = header.matcher(line);
       Matcher mtime = time.matcher(line);
       Matcher mid = id.matcher(line);
       Matcher mcontent = content.matcher(line);
       Matcher srtype = srt.matcher(line);
       if(line.startsWith("Content-Disposition:")&&!srtype.find()){
	    	   br.close();
	    	   subtitleFile.delete();
	    	   return Response.status(Status.BAD_REQUEST)
			    		.entity(new ErrorMessage("error", "Unsupported ContentType! Try uploading an srt file.."))
			    		.type(MediaType.APPLICATION_JSON).build();  
       }
       if(!mhead.find()){
         if(mid.find()){
           if(counter!=0){
             subtitle.setSubtitle(subcontent._array);
             SubtitleDao.instance.getSubtitles().add(subtitle);
             subtitle = new SubtitleImpl();
             subcontent=new StringArray();
           }
         }else if (mtime.find()) {
           String temp; 
           temp=line.replaceAll("-->"," ");
           temp = temp.trim().replaceAll(" +"," ");
           String[] subtime = temp.split(" ");
           subtitle.setStartTime(new TimeImpl(subtime[0]));
           subtitle.setStopTime(new TimeImpl(subtime[1]));
        }else if(mcontent.find()){
           String temp;
           counter++;
           temp=line.trim();
           subcontent.add(temp);
        }
       }  
     }
     subtitle.setSubtitle(subcontent._array);
     SubtitleDao.instance.getSubtitles().add(subtitle);
     br.close();
   } catch (IOException e) {
	   return Response.status(Status.BAD_REQUEST)
				.entity(new ErrorMessage("error", "IOException"))
				.type(MediaType.APPLICATION_JSON).build();
   }
   return Response.ok().build();
 }
 
 /**Consumes a JSON array, deserializes it and 
  * then adds each deserialized subtitle into the data structure
 * @param subtitles to be deserialized
 * @return status OK or INTERNAL_SERVER_ERROR when an exception occurs
 */
 @Path("insertSubtitles/")
 @POST
 @Consumes(MediaType.APPLICATION_JSON)
 public Response insertSubtitlesJson(JsonParser subtitles) {
	 ObjectMapper mapper = new ObjectMapper();
	 try {
		 SubtitleDao.instance.getSubtitles().clear();
		SubtitleDao.instance.getSubtitles().addAll(mapper.readValue(subtitles, mapper.getTypeFactory().constructCollectionType(Collection.class, SubtitleImpl.class)));
	} catch(JsonProcessingException e){
		return Response.status(Status.BAD_REQUEST)
				.entity(new ErrorMessage("error", "JSON Deserialiser Error"))
				.type(MediaType.APPLICATION_JSON).build();
	}catch (IOException e) {
		return Response.status(Status.BAD_REQUEST)
				.entity(new ErrorMessage("error", "IOException"))
				.type(MediaType.APPLICATION_JSON).build();
	}
	 return Response.ok().build();	 
 }
 
 /**
  * Sends to the client the Json representation 
  * of all the subtitles added so far.
  * 
 * @return return subtitles in a json array format
 */
 @Path("getSubtitles/")
 @GET
 @Produces(MediaType.APPLICATION_JSON)
 public ArrayList<SubtitleImpl> getSubtitlesJson() {
   return SubtitleDao.instance.getSubtitles();
 }
 
 /**POST,DELETE,PUT,GET for an individual subtitle specified by the id
  * 
 * @param id position of subtitle in the data structure
 * @return SubtitleResource 
 */
 @Path("subtitle/{subtitle}")
 public SubtitleResource getSubtitle(@PathParam("subtitle") int id) {
   return new SubtitleResource(uriInfo, request, id);
 }
}
