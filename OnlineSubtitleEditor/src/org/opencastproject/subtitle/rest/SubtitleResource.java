package org.opencastproject.subtitle.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import org.opencastproject.subtitle.errorhandling.ErrorMessage;
import org.opencastproject.subtitle.impl.SubtitleDao;
import org.opencastproject.subtitle.impl.SubtitleImpl;


/**
 * 
 * This class handles requests that 
 * are called by this url http://localhost:8080/OnlineSubtitleEditor/rest/subtitleService/subtitle/{id}
 *
 */
public class SubtitleResource {

  @Context
  UriInfo uriInfo;
  @Context
  Request request;
  int id;
  
  public SubtitleResource(UriInfo uriInfo, Request request, int id) {
    this.uriInfo = uriInfo;
    this.request = request;
    this.id = id;
  }
  
  
 /**
 * Returns the Json representation of the subtitle.
 * 
 * @return
 */
 @GET
 @Produces(MediaType.APPLICATION_JSON)
 public Response getSubtitle() {
	 SubtitleImpl subtitle;
	 try{
		 subtitle = SubtitleDao.instance.getSubtitles().get(id-1); 
	 }catch(IndexOutOfBoundsException e){
		 return Response.status(Status.NOT_FOUND)
		    		.entity(new ErrorMessage("error", "The requested subtitle does not exist."))
		    		.type(MediaType.APPLICATION_JSON).build(); 
	 }
	 return Response.ok()
	    		.entity(subtitle)
	    		.type(MediaType.APPLICATION_JSON).build();
 }
  
/**
 * Consumes subtitle Json represantation
 * and adds the subtitle in the data model.
 * 
 * @param subtitle
 * @return Response
 */
  @POST
  @Consumes(MediaType.APPLICATION_JSON)
  public Response postSubtitle(SubtitleImpl subtitle){
	if(subtitle==null){
	return Response.status(Status.BAD_REQUEST)
		.entity(new ErrorMessage("error", "Json Deserializer Error"))
		.type(MediaType.APPLICATION_JSON).build();
	}
	if("00:99:00.000".equals(subtitle.getStartTime())){
		return Response.status(Status.BAD_REQUEST)
				.entity(new ErrorMessage("error", "Missing Parameter [starttime]"))
				.type(MediaType.APPLICATION_JSON).build();	
	}else if("99:99:99.999".equals(subtitle.getStartTime())){
		return Response.status(Status.BAD_REQUEST)
				.entity(new ErrorMessage("error", "Invalid Time Parameter [starttime]"))
				.type(MediaType.APPLICATION_JSON).build();
	}
	if("00:99:00.000".equals(subtitle.getStopTime())){
		return Response.status(Status.BAD_REQUEST)
				.entity(new ErrorMessage("error", "Missing Parameter [endtime]"))
				.type(MediaType.APPLICATION_JSON).build();	
	}else if("99:99:99.999".equals(subtitle.getStopTime())){
		return Response.status(Status.BAD_REQUEST)
				.entity(new ErrorMessage("error", "Invalid Time Parameter [endtime]"))
				.type(MediaType.APPLICATION_JSON).build();
	}
	if("".equals(subtitle.getSubtitle())){
		return Response.status(Status.BAD_REQUEST)
				.entity(new ErrorMessage("error", "Missing Parameter [content]"))
				.type(MediaType.APPLICATION_JSON).build();	
	}
    return postAndGetResponse(subtitle);
  }

/**
 * Consumes subtitle Json represantation
 * and updates the subtitle in the data model.
 * @param subtitle
 * @return Response
 */
@PUT
@Consumes({MediaType.APPLICATION_JSON})
public Response putSubtitle(SubtitleImpl subtitle) {
	if(subtitle==null){
		return Response.status(Status.BAD_REQUEST)
			.entity(new ErrorMessage("error", "Json Deserializer Error"))
			.type(MediaType.APPLICATION_JSON).build();
		}
	if("00:99:00.000".equals(subtitle.getStartTime())){
		return Response.status(Status.BAD_REQUEST)
				.entity(new ErrorMessage("error", "Missing Parameter [starttime]"))
				.type(MediaType.APPLICATION_JSON).build();	
	}else if("99:99:99.999".equals(subtitle.getStartTime())){
		return Response.status(Status.BAD_REQUEST)
				.entity(new ErrorMessage("error", "Invalid Time Parameter [starttime]"))
				.type(MediaType.APPLICATION_JSON).build();
	}
	if("00:99:00.000".equals(subtitle.getStopTime())){
		return Response.status(Status.BAD_REQUEST)
				.entity(new ErrorMessage("error", "Missing Parameter [endtime]"))
				.type(MediaType.APPLICATION_JSON).build();	
	}else if("99:99:99.999".equals(subtitle.getStopTime())){
		return Response.status(Status.BAD_REQUEST)
				.entity(new ErrorMessage("error", "Invalid Time Parameter [endtime]"))
				.type(MediaType.APPLICATION_JSON).build();
	}
	if("".equals(subtitle.getSubtitle())){
		return Response.status(Status.BAD_REQUEST)
				.entity(new ErrorMessage("error", "Missing Parameter [content]"))
				.type(MediaType.APPLICATION_JSON).build();	
	}
  return putAndGetResponse(subtitle);
}
  
  /**
 * Deletes a subtitle.
 */
@DELETE
  public Response deleteSubtitle() {
	try{
		SubtitleDao.instance.getSubtitles().remove(id-1);
	}catch(IndexOutOfBoundsException e){
		
		return Response.status(Status.NOT_FOUND)
	    		.entity(new ErrorMessage("error", "The requested subtitle does not exist."))
	    		.type(MediaType.APPLICATION_JSON).build();
	}
	return Response.ok().build();
	
  }
  
  /**
   * Adds a subtitle and builds subtitle url path.
   * 
 * @param subtitle
 * @return Response
 */
private Response postAndGetResponse(SubtitleImpl subtitle) {
    Response res;
    res = Response.ok().build();
    try{
    	SubtitleDao.instance.getSubtitles().add(id-1,subtitle);
    }catch(IndexOutOfBoundsException e){
    	return Response.status(Status.BAD_REQUEST)
	    		.entity(new ErrorMessage("error", "The requested subtitle could "
	    				+ "not be inserted to the selected position."))
	    		.type(MediaType.APPLICATION_JSON).build();
    }
    return res;
  }
  
  /**
   * Updates an existing subtitle.
   * 
 * @param subtitle
 * @return Response
 */
private Response putAndGetResponse(SubtitleImpl subtitle) {
	    Response res;
	    res = Response.ok().build();
	    try{
	    	SubtitleDao.instance.getSubtitles().remove(id-1);
	    	SubtitleDao.instance.getSubtitles().add(id-1,subtitle);
	    }catch(IndexOutOfBoundsException e){
	    	return Response.status(Status.BAD_REQUEST)
		    		.entity(new ErrorMessage("error", "The requested subtitle could "
		    				+ "not be inserted to the selected position."))
		    		.type(MediaType.APPLICATION_JSON).build();
	    }
	    return res;
	  }
}
