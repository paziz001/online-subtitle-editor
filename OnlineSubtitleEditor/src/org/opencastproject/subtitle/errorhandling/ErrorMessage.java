package org.opencastproject.subtitle.errorhandling;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * @author Philippos Aziz
 *
 */
@JsonPropertyOrder({"status","message"})
public class ErrorMessage {
	
	/** contains the same HTTP Status code returned by the server */
	@JsonProperty("status")
	String status;
	
	/** message describing the error*/
	@JsonProperty("message")
	String message;
	
	/**Constructor for error message
	 * @param status status of the message to be sent
	 * @param message actual error message that will be desplayed
	 */
	public ErrorMessage(String status,String message) {
		this.status=status;
		this.message=message;
	}
	
	public String getStatus() {
	   return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	
}