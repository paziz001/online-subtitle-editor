package org.opencastproject.subtitle.impl;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

/**
 * Class for custom Json deserializing of SubtitleImpl class.
 *
 */
public class SubtitleDeserializer extends JsonDeserializer<SubtitleImpl> {
 
    /* (non-Javadoc)
     * @see com.fasterxml.jackson.databind.JsonDeserializer#deserialize(com.fasterxml.jackson.core.JsonParser, com.fasterxml.jackson.databind.DeserializationContext)
     */
    @Override
    public SubtitleImpl deserialize(JsonParser jp, DeserializationContext ctxt)
    {
    	JsonNode node;
    	String starttime;
    	String endtime;
    	String content;
    	try{
	        node = jp.getCodec().readTree(jp);
	        if(node.get("endtime")==null 
	        		|| node.get("starttime")==null 
	        		|| node.get("content").asText()==null){
	        	return null;
	        }
	        Pattern time = Pattern.compile("^[0-9]{2}:[0-5]{1}[0-9]{1}:[0-5]{1}[0-9]{1}[.]{1}[0-9]{3}$");  
	        starttime = node.get("starttime").asText();	
	        Matcher mtime = time.matcher(starttime);
	        if(starttime==""){
	        	starttime="00:99:00.000";
	        }else{
	        	if(!mtime.find()){
	        		starttime="99:99:99.999";
		        }
	        }
	        endtime=node.get("endtime").asText();
	        mtime = time.matcher(endtime);
	        
	        if(endtime==""){
	        	endtime="00:99:00.000";
	        }else{
	        	if(!mtime.find()){
	        		endtime="99:99:99.999";
		        }
	        }
	        	
	        content = node.get("content").asText();
    	}catch(JsonProcessingException e){
    		return null;
    	}catch(IOException e){
    		return null;
    	}
        return new SubtitleImpl(new TimeImpl(starttime),new TimeImpl(endtime),content.split("/\n*") );
    }
}