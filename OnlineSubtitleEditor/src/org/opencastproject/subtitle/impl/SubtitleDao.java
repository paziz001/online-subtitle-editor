package org.opencastproject.subtitle.impl;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * Temporary storage for subtitles
 *
 */
@XmlRootElement
public enum SubtitleDao {
  instance;
  

  private ArrayList<SubtitleImpl> subtitleProvider = new ArrayList<SubtitleImpl>();
  
  private SubtitleDao(){
  }
  
  /**
   * Gets all subtitles.
   * @return all subtitles 
   */
  public ArrayList<SubtitleImpl> getSubtitles(){
    return subtitleProvider;
  }

}