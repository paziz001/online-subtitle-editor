package org.opencastproject.subtitle.impl;





import org.opencastproject.subtitle.api.Time;

public class TimeImpl implements Time {

  private int hours;
  private int minutes;
  private int seconds;
  private int milliseconds;
  
  public TimeImpl(String time) {
    String[] divtime = time.split("[:.,]");
    this.hours=Integer.valueOf(divtime[0]);
    this.minutes=Integer.valueOf(divtime[1]);
    this.seconds=Integer.valueOf(divtime[2]);
    this.milliseconds=Integer.valueOf(divtime[3]);
  }
  public TimeImpl(){
    
  }
  
  @Override
  public int getHours() {
    return this.hours;
  }
  @Override
  public int getMinutes() {
    return this.minutes;
  }
  @Override
  public int getSeconds() {
    return this.seconds;
  }
  @Override
  public int getMilliseconds() {
    return this.milliseconds;
  }
  @Override
  public void setHours(int hours) {
    this.hours=hours;
    
  }
  @Override
  public void setMinutes(int minutes) {
    this.minutes=minutes;
  }
  @Override
  public void setSeconds(int seconds) {
    this.seconds=seconds;
  }
  @Override
  public void setMilliseconds(int milliseconds) {
    this.milliseconds=milliseconds;
    
  }
  @Override
  public String toString(){
    String shours;
    String sminutes;
    String sseconds;
    String smilliseconds;
    
    if(this.hours>=10){
      shours = ""+this.hours;
    }else{
      shours = "0"+this.hours;
    }
    
    if(this.minutes>=10){
      sminutes = ""+this.minutes;
    }else{
      sminutes = "0"+this.minutes;
    }
    
    if(this.seconds>=10){
      sseconds = ""+this.seconds;
    }else{
      sseconds = "0"+this.seconds;
    }
    
    if(this.milliseconds>=100){
      smilliseconds = ""+this.milliseconds;
    }else{
      if(this.milliseconds>=10){
        smilliseconds = "0"+this.milliseconds;
      }else{
        smilliseconds = "00" + this.milliseconds;
      }
       
    }
    
    return shours+":"+sminutes+":"+sseconds+"."+smilliseconds;
    
  }
  
}
