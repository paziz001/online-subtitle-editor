package org.opencastproject.subtitle.impl;

import org.opencastproject.subtitle.api.Subtitle;
import org.opencastproject.subtitle.impl.TimeImpl;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@JsonPropertyOrder({"starttime","endtime","content"})
@JsonDeserialize(using = SubtitleDeserializer.class)
public class SubtitleImpl implements Subtitle{
 
  private TimeImpl startTime;
  private TimeImpl stopTime;
  private String[] subtitleText;
  
  public SubtitleImpl(TimeImpl startTime,TimeImpl stopTime, String[] subtitleText) {
    this.startTime=startTime;
    this.stopTime=stopTime;
    this.subtitleText=subtitleText;
  }
  
  public SubtitleImpl(){
    
  }

  @JsonProperty("starttime")
  public String getStartTime() {
    return this.startTime.toString();
  }

  @JsonProperty("endtime")
  public String getStopTime() {
    return this.stopTime.toString();
  }

  @JsonProperty("content")
  public String getSubtitle() {
    String temp="";
    for(String s:this.subtitleText){
    	if(s!=null)
    		temp+=s+"\n";
    }
    return temp.substring(0, temp.length()-1);
  }

  public void setStartTime(TimeImpl startTime) {
    this.startTime=startTime;
    
  }

  public void setStopTime(TimeImpl stopTime) {
    this.stopTime=stopTime;
    
  }
  

  public void setSubtitle(String[] subtitle) {
    this.subtitleText=subtitle;
    
  }
  @Override
  public String toString(){
    StringBuilder buff = new StringBuilder();
    buff
    .append(this.getStartTime().replace('.', ','))
    .append(" --> ")
    .append(this.getStopTime().replace('.', ','))
    .append("\r\n");
    for(int i=0;i<this.subtitleText.length;i++){
      if(this.subtitleText[i]!=null)   	
    	  buff.append(this.subtitleText[i]+"\r\n");
    }
    return buff.toString();
  }

 

  
}
