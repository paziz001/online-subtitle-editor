var example_video=Popcorn('#video-container');
var regtime = new RegExp('[0-9]{1}:[0-5]{1}[0-9]{1}:[0-5]{1}[0-9]{1}\.[0-9]{3}');
var FIRST_LOAD=1;
$('.dropdown').mouseenter(function(){
    $(this).addClass('open');
    $(this).addClass('open');
});
$('.dropdown').mouseleave(function(){
    $(this).removeClass('open');
});
/****************table initialisation*********************/
var table=$('#subtitles-table').DataTable({
    paging: false,
    scrollY: 700,
    ordering:false,
    searching:false,
    bInfo: false
});
table.columns('.ID').order('asc');

$('body').on('click', '#subtitles-table tbody tr', function () {
    if ( $(this).hasClass('selected') ) {
        $(this).removeClass('selected');
        $(this).children('.subContent').children('.delete_row').css('visibility','hidden');
    }
    else {
        $('tr.selected').children('.subContent').children('.delete_row').css('visibility','hidden');
        $(this).children('.subContent').children('.delete_row').css('visibility','visible');
        $('#subtitles-table tr.selected').removeClass('selected');
        $(this).addClass('selected');
        $('#start-time-input').val($('tr.selected .subStart').html());
        $('#end-time-input').val($('tr.selected .subEnd').html());
        $('#content-input').val($('tr.selected .subContent span').html());
        $('#selected-action').html("Choose");
        $('#confirm-wrapper .errormsg').remove();
    }
});

/*************input handlers**************************/

var stto = null;
var stint = null;
$('#arrow-up-starttime').on('mousedown',function(){
    var milliseconds = srtToMilliseconds($('#start-time-input').val());
    $('#start-time-input').val(millisecondsToSrtTime(milliseconds+100));
    stto = setTimeout(function(){
        stint = setInterval(function(){
            milliseconds = srtToMilliseconds($('#start-time-input').val());
            $('#start-time-input').val(millisecondsToSrtTime(milliseconds+100));
        },75);
    },500);
    }).on("mouseup",function() {
    clearTimeout(stto);
    clearInterval(stint);
    });

$('#arrow-down-starttime').on('mousedown',function(){
    if(srtToSeconds($('#start-time-input').val())>0) {
        var milliseconds = srtToMilliseconds($('#start-time-input').val());
        $('#start-time-input').val(millisecondsToSrtTime(milliseconds - 100));
        stto = setTimeout(function () {
            stint = setInterval(function () {
                if(srtToSeconds($('#start-time-input').val())>0) {
                    milliseconds = srtToMilliseconds($('#start-time-input').val());
                    $('#start-time-input').val(millisecondsToSrtTime(milliseconds - 100));
                }
            }, 75);
        }, 500);
    }
}).on("mouseup",function() {
    clearTimeout(stto);
    clearInterval(stint);
});

var endto=null;
var endint=null;

$('#arrow-up-endtime').on('mousedown',function(){
        var milliseconds = srtToMilliseconds($('#end-time-input').val());
        $('#end-time-input').val(millisecondsToSrtTime(milliseconds + 100));
        endto = setTimeout(function () {
            endint = setInterval(function () {
                milliseconds = srtToMilliseconds($('#end-time-input').val());
                $('#end-time-input').val(millisecondsToSrtTime(milliseconds + 100));
            }, 75);
        }, 500);
}).on("mouseup",function() {
    clearTimeout(endto);
    clearInterval(endint);
});

$('#arrow-down-endtime').on('mousedown',function(){
    if(srtToSeconds($('#end-time-input').val())>0) {
        var milliseconds = srtToMilliseconds($('#end-time-input').val());
        $('#end-time-input').val(millisecondsToSrtTime(milliseconds - 100));
        endto = setTimeout(function () {
            endint = setInterval(function () {
                if(srtToSeconds($('#end-time-input').val())>0) {
                    milliseconds = srtToMilliseconds($('#end-time-input').val());
                    $('#end-time-input').val(millisecondsToSrtTime(milliseconds - 100));
                }
            }, 75);
        }, 500);
    }
}).on("mouseup",function() {
    clearTimeout(endto);
    clearInterval(endint);
});



$('#start-time-input').focusout(function(){
    if($('#start-time-input').val().match(regtime)==null){
        $('#start-time-input').val('00:00:00.000');
    }
});

$('#end-time-input').focusout(function(){
    if($('#end-time-input').val().match(regtime)==null){
        $('#end-time-input').val('00:00:00.000');
    }
});

$('body #add-options a').click(function(){
   $('.dropup button #selected-action').html($(this).html());
    $('#confirm-wrapper .errormsg').remove();

});

/*************************************Remove subtitle row*****************************************/
/**
 * When a subtitle is removed from the table an ajax call is made to insert all the table's subtitles
 * in the back-end's data structure.Then another ajax call is made to delete the subtitle from the data
 * structure.
 */
$('body').on('click', '#subtitles-table tbody tr.selected .delete_row',function(){
    var id=$(this).closest("td").siblings(".subID").html();
    if($(this).closest("td").siblings(".subID").html()==$('#subtitles-table tbody tr').length){
        var jsontosent=[];
        toJsonArray(jsontosent);
        $.ajax({
            url:"http://localhost:8080/OnlineSubtitleEditor/rest/subtitleService/insertSubtitles/",
            method:'POST',
            contentType:"application/json; charset=utf-8",
            dataType:"text",
            data:JSON.stringify(jsontosent),
            success:function(){
                $.ajax({
                    url:"http://localhost:8080/OnlineSubtitleEditor/rest/subtitleService/subtitle/"+id,
                    method:'DELETE',
                    success:function(){
                        $(this).closest('tr').remove();
                        if(id==1){
                            $('#subtitles-table tbody').remove();
                        }
                        $('#start-time-input').val('');
                        $('#start-time-input').val('00:00:00.000');
                        $('#end-time-input').val('00:00:00.000');
                        $('#content-input').val('');
                        $('#selected-action').html("Choose");
                        updateTable();
                    }
                });
            }
        });
    }
    else{
        var jsontosent=[];
        toJsonArray(jsontosent);
        $.ajax({
            url:"http://localhost:8080/OnlineSubtitleEditor/rest/subtitleService/insertSubtitles/",
            method:'POST',
            contentType:"application/json; charset=utf-8",
            dataType:"text",
            data:JSON.stringify(jsontosent),
            success:function(){
                $.ajax({
                    url:"http://localhost:8080/OnlineSubtitleEditor/rest/subtitleService/subtitle/"+id,
                    method:'DELETE',
                    success:function(){
                        $(this).closest('tr').remove();
                        $('#start-time-input').val('');
                        $('#start-time-input').val('00:00:00.000');
                        $('#end-time-input').val('00:00:00.000');
                        $('#content-input').val('');
                        $('#selected-action').html("Choose");
                        updateTable();

                    }
                });
            }
        });
    }
    if(FIRST_LOAD==1){
        $(this).closest('tr').remove();
        $('#start-time-input').val('');
        $('#start-time-input').val('00:00:00.000');
        $('#end-time-input').val('00:00:00.000');
        $('#content-input').val('');
        $('#selected-action').html("Choose");
        updateTable();
    }
});

$('#confirm-action').click(function(){
    if($('#selected-action').html()=="Choose"){
        $('#confirm-wrapper #errormsg').remove();
        $('#confirm-wrapper').append('<p class="errormsg" style="color:red">Please choose an option.</p>');
        return;
    }else{
        if($('#subtitles-table tr.selected').html()==undefined
        &&$('#selected-action').html()!="Add new subtitle"){
            $('#confirm-wrapper #errormsg').remove();
            $('#confirm-wrapper').append('<p id="errormsg" style="color:red">You have to select a subtitle first.</p>');
            return;
        }
    }

    $('#confirm-wrapper #errormsg').remove();

    if($('#subtitles-table tbody').html()==undefined){
        $('#subtitles-table').append('<tbody></tbody>');
    }
    if($('#selected-action').html()=="Add new subtitle"){
        addSubtitle();
    }else if($('#selected-action').html()=="Edit selected subtitle"){
        editSelectedSub();
    }else if($('#selected-action').html()=="Insert before selected subtitle"){
        insertBeforeSelectedSub();
    }else if($('#selected-action').html()=="Insert after selected subtitle"){
        insertAfterSelectedSub();
    }

});
/**
 * When the break subtitle button is clicked the line is break by new lines every 7 spaces.
 */
$('#break-subtitle').click(function(){
    var trimmedline=$('#content-input').val().replace( /  +/g, ' ' );
    var contentarray=trimmedline.split(/ /);
    var breakedsubtitle="";
    $.each(contentarray,function(key,value){
        if(!parseInt(key%7)==0){
            breakedsubtitle+=value;
            breakedsubtitle+=" ";
        }else{
            if(key!=0) {
                breakedsubtitle += value;
                breakedsubtitle += "\n";
            }else{
                breakedsubtitle+=value;
                breakedsubtitle+=" ";
            }
        }
    });
    $('#content-input').val(breakedsubtitle);
});



/**********video javascript**************/
$(document).ready(function(){
    $.ajax({
        url:"http://localhost:8080/OnlineSubtitleEditor/rest/subtitleService/emptySubtitleDao/",
        method:'DELETE',
        success:function(){
        }
    });
    $("#start-time-input").mask("99:59:59.999");
    $("#end-time-input").mask("99:59:59.999");
    $('#subtitles-table tbody').remove();
    $('#subtitles-table').append('<tbody></tbody>');
    $.getJSON("subtitles_example/google-devs",function(data){
        $.each(data,function(key,value){
            if((key+1)%2!=0)
                $('#subtitles-table tbody').append('<tr class="odd" id="tr_'+(key+1)+'" role="row"></tr>');
            else
                $('#subtitles-table tbody').append('<tr class="even" id="tr_'+(key+1)+'" role="row"></tr>');

            $('#tr_'+(key+1)).append('<td class="subID">'+(key+1)+'</td>');
            $('#tr_'+(key+1)).append('<td class="subStart">'+value.starttime+'</td>');
            $('#tr_'+(key+1)).append('<td class="subEnd">'+value.endtime+'</td>');
            $('#tr_'+(key+1)).append('<td class="subContent"><span>' + value.content +
            '</span><button type="button" style="visibility: hidden" class="delete_row close" aria-label="Close">' +
            '<span aria-hidden="true">&times;</span></button></td>');
        });
        $('#subtitles-table tbody tr').each(function (key,value) {
            example_video.subtitle({
                start: srtToSeconds($('#tr_'+(key+1)+' .subStart').html()),
                end: srtToSeconds($('#tr_'+(key+1)+' .subEnd').html()),
                target:"subtitle",
                text: '<span>'+$('#tr_'+(key+1)+' .subContent span').html()+'</span>'
            });
        } );



    });
});


/***********************************************************
 *
 * nav Subtitles options
 *
 * *********************************************************/



/*********************new Subtitles************************/
/**
 * When the nav option Subtitles->New Subtitles is clicked a request
 * is made to clear the back-end's data structure. Then an ajax call is made
 * with an example subtitle to be inserted to the back-end and at last the subtitle is inserted to the table.
 */
$('#new-subtitles-li').click(function(){
    FIRST_LOAD=0;
    $('#start-time-input').val('00:00:00.000');
    $('#end-time-input').val('00:00:00.000');
    $('#content-input').val();
    $('#selected-action').html("Choose");
    var id=1;
    var start_time="00:00:00.000";
    var end_time="00:00:03.000";
    var content="Insert a content";
    updateTable();
    var jsonaddsub= $.parseJSON('{"starttime":"'+start_time+'","endtime":"'+end_time+'","content":"'+content+'"}');
    $.ajax({
        url:"http://localhost:8080/OnlineSubtitleEditor/rest/subtitleService/emptySubtitleDao/",
        method:'DELETE',
        success:function(){
            $.ajax({
                url:"http://localhost:8080/OnlineSubtitleEditor/rest/subtitleService/subtitle/"+id,
                method:'POST',
                data:JSON.stringify(jsonaddsub),
                contentType:"application/json; charset=utf-8",
                dataType:"text",
                success:function(){
                    $('#subtitles-table tbody').remove();
                    $('#subtitles-table').append('<tbody></tbody>');
                    $('#subtitles-table tbody').append('<tr id="tr_'+1+'"></tr>');
                    $('#subtitles-table tbody tr').append('<td class="subID">1</td>');
                    $('#subtitles-table tbody tr').append('<td class="subStart">00:00:00.000</td>');
                    $('#subtitles-table tbody tr').append('<td class="subEnd">00:00:03.000</td>');
                    $('#subtitles-table tbody tr').append('<td class="subContent"><span>Insert a content' +
                    '</span><button type="button" style="visibility: hidden" class="delete_row close" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span></button></td>');
                    $('#subtitles-table tbody tr .subID').html();
                    updateTable();
                }
            });
        }
    });


});


/*********************open existing Subtitles************************/
/**
 * When the nav option Subtitles->Add Existing Subtitles is clicked a request
 * is with a subtitle file is made to update the data structure and then another ajax
 * call requesting all the subtitles, that were inserted in the data structure,is made
 * so they can  be inserted in the table.
 */
$('#existing-subtitles-li').click(function(){
    $( "#myModal").unbind( "hide.bs.modal" );
    $('#myModal').on('hide.bs.modal',function(){
        $('#myfileinput').fileinput('reset');
        $('#myModal .modal-body').empty();
        $('#myModal .modal-title').empty();
        $('#myModal .modal-footer button.btn-primary').remove();
        $('body').css('cursor','default');
    });
    $('#myModal .modal-body').append('<input type="file" accept=".srt" id="myfileinput">');
    $('#myfileinput').fileinput({
        'allowedFileExtensions':['srt'],
        'showPreview':false,
        'showUpload':false,
        'showRemove':false,
        'msgValidationError':"File Upload Error: srt file needed",
        'uploadUrl':"http://localhost:8080/OnlineSubtitleEditor/rest/subtitleService/addExistingSubtitleFile/",
        'ajaxSettings':{contentType:'multipart/form-data'}
    });
    $('#myfileinput').on('fileloaded', function(event, file, previewId, index, reader) {
        $('#myModal .modal-footer button.btn-primary').removeAttr('disabled');
    });
    $('#myfileinput').on('filebatchuploadcomplete', function(event, data, previewId, index) {
        $('body').css('cursor','wait');
        $.getJSON("http://localhost:8080/OnlineSubtitleEditor/rest/subtitleService/getSubtitles",function(data){
            $('#subtitles-table tbody').remove();
            $('#subtitles-table').append('<tbody></tbody>');
            $.each(data,function(key,value){
                if((key+1)%2!=0)
                    $('#subtitles-table tbody').append('<tr class="odd" id="tr_'+(key+1)+'" role="row"></tr>');
                else
                    $('#subtitles-table tbody').append('<tr class="even" id="tr_'+(key+1)+'" role="row"></tr>');

                $('#tr_'+(key+1)).append('<td class="subID">'+(key+1)+'</td>');
                $('#tr_'+(key+1)).append('<td class="subStart">'+value.starttime+'</td>');
                $('#tr_'+(key+1)).append('<td class="subEnd">'+value.endtime+'</td>');
                $('#tr_'+(key+1)).append('<td class="subContent"><span>' + value.content +
                '</span><button type="button" style="visibility: hidden" class="delete_row close" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span></button></td>');
            });
            updateTable();
            $('#myModal').modal('hide');


        });
    });
    $('#myModal .modal-footer').append('<button type="button" class="btn btn-primary">Ok</button>');
    $('#myModal .modal-footer button.btn-primary').attr('disabled','disabled');
    $('#myModal .modal-footer button.btn-primary').click(function(e){
        $('#myfileinput').fileinput('upload');
        e.preventDefault();
    });
    $('#myModal .modal-title').html("Open Existing Subtitles");
    $('#myModal').modal('show');
});

/*********************Download Subtitles************************/
/**
 * When the nav option Subtitles->Download Subtitles is clicked a request
 * is made to update the data structure based on the subtitles that are in the
 * table and then another ajax call is made requesting the subtitles file
 * that contains the subtitles that are in the table so far.
 */
$('#save-subtitles-li').click(function(e){
    e.preventDefault();
    var jsontosent=[];
    toJsonArray(jsontosent);
    $.ajax({
        url:"http://localhost:8080/OnlineSubtitleEditor/rest/subtitleService/insertSubtitles/",
        method:'POST',
        contentType:"application/json; charset=utf-8",
        dataType:"text",
        data:JSON.stringify(jsontosent),
        success:function(){
            document.getElementById('subtitle-file').src = 'http://localhost:8080/OnlineSubtitleEditor/rest/subtitleService/downloadSubtitleFile';
        }
    });

});

/*********************Open Video************************/
/**
 * The modal that is toggled when the nav option Video->Open Video is clicked.
 * The modal containes a file browser for choosing a video and loading it on the page;
 */
$('#openvideo-li').click(function(){
    $( "#myModal").unbind( "hide.bs.modal" );
    $('#myModal').on('hide.bs.modal',function(){
        $('#myvideoinput').fileinput('reset');
        $('#myModal .modal-body').empty();
        $('#myModal .modal-title').empty();
        $('#myModal .modal-footer button.btn-primary').remove();
    });
    $('#myModal .modal-body').append('<input type="file" accept=".mp4" id="myvideoinput">');
    $('#myvideoinput').fileinput({
        'allowedFileExtensions':['mp4'],
        'showPreview':false,
        'showUpload':false,
        'showRemove':false,
        'msgValidationError':"File Upload Error: mp4 file needed"
    });
    $('#myModal .modal-footer').append('<button type="button" class="btn btn-primary">Ok</button>');
    $('#myModal .modal-footer button.btn-primary').attr('disabled','disabled');
    $('#myvideoinput').on('fileloaded', function(event, file, previewId, index, reader) {
        $('#myModal .modal-footer button.btn-primary').removeAttr('disabled');
    });
    $('#myModal .modal-footer button.btn-primary').click(function(e){
        var fileInput = document.getElementById('myvideoinput');
        var video = document.getElementById('video-container');
        var fileUrl = window.URL.createObjectURL(fileInput.files[0]);
        video.src = fileUrl;
        $('#myModal').modal('hide');
        e.preventDefault();
    });
    $('#myModal .modal-title').html("Open Video");
    $('#myModal').modal('show');
});


/*********************Help************************/

/**
 * The modal that is toggled when the nav option About->Help is clicked.
 * The modal contains tips for using the editor
 */
$('#help-li').click(function(){
    $( "#myModal").unbind( "hide.bs.modal" );
    $('#myModal .modal-title').html("Help");
    $('#myModal .modal-body').html("<p>The subtitle file format that is accepted is .srt and the video file is .mp4.</p>" +
    "<p>You can add a subtitle at the and of the subtitles table by choosing the Add new  subtitle option.</p>" +
    "<p>You can edit a subtitle by selecting a subtitle from the subtitles table by choosing the Edit selected Subtitle option.</p>" +
    "<p>You can insert a subtitle after a selected subtitle from the subtitles table by choosing the Insert after selected subtitle option.</p>" +
    "<p>You can insert a subtitle before a selected subtitle from the subtitles table by choosing the Insert before selected subtitle option.</p>"+
    "<p>You can edit the time input by pressing on the field or either by having one of the arrows near the input pressed to increase or decrease the time.</p>");
    $('#myModal').on('hide.bs.modal',function(){
        $('#myModal .modal-body').empty();
        $('#myModal .modal-title').empty();
        $('#myModal .modal-body').after('<div class="modal-footer"></div>');
        $('#myModal .modal-footer').append('<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>');
        $('#myModal .modal-body').css('background-color','#FFFFFF');
        $('#myModal .modal-body').css('color','inherit');
    });
    $('#myModal .modal-body').css('background-color','#595241');
    $('#myModal .modal-body').css('color','#FFFFFF');
    $('#myModal .modal-footer').remove();
    $('#myModal').modal('show');
});
/**
 * The modal that is toggled when the nav option About->About Online Subtitle Editor is clicked.
 * The modal contains info about the opencast project licence.
 */
$('#about-li').click(function(){
    $( "#myModal").unbind( "hide.bs.modal" );
    $('#myModal .modal-title').html("About Online Subtitles Editor");
    $('#myModal .modal-body').append('<div class="container"></div><div class="row"><div class="col-lg-push-1 col-md-push-1 col-lg-6 col-md-6"><img alt="matterhorn logo" src="images/matterhorn_logo.png" style="margin-top:-4px;margin-left:20px" >' +
    '<p  style="color:#830517;margin-left:20px">Online Subtitles Editor</p><a href="http://creativecommons.org/licenses/by/3.0/us/" target="_blank" ><img alt="license logo" src="images/CC-BY_icon.svg.png" style="margin-top:-5px;margin-left:36px" ></a></div>'+
    '<div class="col-lg-6 col-md-6"><span class="about-body-details"><p><a href="http://www.opencast.org" target="_blank">Opencast Projects</a> are licensed under a ' +
    '<a href="http://creativecommons.org/licenses/by/3.0/us/" target="_blank">Creative Commons Attribution 3.0 United States License</a> and source is available under ' +
    '<a href="http://opencast.jira.com/wiki/display/MHDOC/License+Information" target="_blank">Educational Community License 2.0</a>. Permissions for commercial use of Opencast Matterhorn\'s logo and name may be available '+
    '<a href="http://www.opencast.org/contact" target="_blank">on request</a>.</p></span></div></div></div>');
    $('#myModal').on('hide.bs.modal',function(){
        $('#myModal .modal-body').empty();
        $('#myModal .modal-title').empty();
        $('#myModal .modal-body').after('<div class="modal-footer"></div>');
        $('#myModal .modal-footer').append('<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>');
        $('#myModal .modal-body').css('background-color','#FFFFFF');
        $('#myModal .modal-body').css('color','inherit');
    });
    $('#myModal .modal-body').css('background-color','#595241');
    $('#myModal .modal-body').css('color','#FFFFFF');
    $('#myModal .modal-footer').remove();
    $('#myModal').modal('show');
});

/***************************
 *
 *
 *functions
 *
 *
 ***************************/

/**
 * This function is responsible for adding a new subtitle. This function contains validations about the
 * correctness of the time inputs based on the time inputs of its nearest subtitles.It also has ajax calls
 * for adding the subtitle to the back end.
 */

function addSubtitle(){
    var key = $('#subtitles-table tbody tr').length;
    var id;
    var stimesec=srtToSeconds($('#start-time-input').val());
    var etimesec=srtToSeconds($('#end-time-input').val());
    var prevsubend;
    var success=0;
    if(key==0){
        id=0;
    }else{
        id=key;
    }

    if(id==0){
        if(stimesec>etimesec){
            swal('Start time overlaps end time by '+(stimesec-etimesec).toFixed(3)+' seconds');
        }else{
            success=1;
        }
    }else{
        prevsubend=srtToSeconds($('tr#tr_'+id+' .subEnd').html());
        if(stimesec>etimesec){
            swal('Start time overlaps end time by '+(stimesec-etimesec).toFixed(3)+' seconds');
        }else if (stimesec<prevsubend){
            swal('Previous subtitle overlaps start time by '+(prevsubend-stimesec).toFixed(3)+' seconds');
        }else{
            success=1;
        }
    }
    if($('#content-input').val()==""){
        success=0;
        swal('Please fill out the subtitle that will be displayed');
    }
    if(success) {
        var id = key+1;
        var start_time = $('#start-time-input').val();
        var end_time = $('#end-time-input').val();
        var content = $('#content-input').val();
        content=content.replace('\n','\\n');
        var jsonaddsub = $.parseJSON('{"starttime":"' + start_time + '","endtime":"' + end_time + '","content":"' + content + '"}');
        var jsontosent=[];
        toJsonArray(jsontosent);
        $.ajax({
            url:"http://localhost:8080/OnlineSubtitleEditor/rest/subtitleService/insertSubtitles/",
            method:'POST',
            contentType:"application/json; charset=utf-8",
            dataType:"text",
            data:JSON.stringify(jsontosent),
            success:function(){
                $.ajax({
                    url: "http://localhost:8080/OnlineSubtitleEditor/rest/subtitleService/subtitle/" + id,
                    method: 'POST',
                    data: JSON.stringify(jsonaddsub),
                    contentType: "application/json",
                    dataType: "text",
                    success: function () {
                        if (id % 2 != 0)
                            $('#subtitles-table tbody').append('<tr class="odd" id="tr_' + id + '" role="row"></tr>');
                        else
                            $('#subtitles-table tbody').append('<tr class="even" id="tr_' + id + '" role="row"></tr>');
                        $('#tr_' + id).append('<td class="subID">' + id + '</td>');
                        $('#tr_' + id).append('<td class="subStart">' + $('#start-time-input').val() + '</td>');
                        $('#tr_' + id).append('<td class="subEnd">' + $('#end-time-input').val() + '</td>');
                        $('#tr_' + id).append('<td class="subContent"><span>' + $('#content-input').val() +
                        '</span><button type="button" class="delete_row close" aria-label="Close">' +
                        '<span aria-hidden="true">&times;</span></button></td>');
                        updateTable();
                        if(id!=1) {
                            $('#subtitles-table tr.selected').children('.subContent').children('.delete_row').css('visibility', 'hidden');
                            $('#subtitles-table tr.selected').removeClass('selected');
                        }
                        $('#subtitles-table #tr_' + id).children('.subContent').children('.delete_row').css('visibility', 'visible');
                        $('#subtitles-table #tr_' + id).addClass('selected');
                        $('#start-time-input').val($('tr.selected .subStart').html());
                        $('#end-time-input').val($('tr.selected .subEnd').html());
                        $('#content-input').val($('tr.selected .subContent span').html());
                        $('#confirm-wrapper .errormsg').remove();
                    }
                });
            }
        });
    }
}
/**
 * This function is responsible for editng a selected subtitlefrom the table. This function
 * contains validations about the correctness of the time inputs based on the time inputs
 * of its nearest subtitles.It also has ajax calls for adding the subtitle to the back end
 */
function editSelectedSub(){
    var id=parseInt($('tr.selected').attr('id').split('_')[1]);
    var stimesec=srtToSeconds($('#start-time-input').val());
    var etimesec=srtToSeconds($('#end-time-input').val());
    var nextsubstart;
    var prevsubend;
    var success=0;
    var key=id;
    if(id==1){
        if($('tr#tr_'+2+' .subStart').html()==undefined){
            if(stimesec>etimesec){
                swal('Start time overlaps end time by '+(stimesec-etimesec)+' seconds');
            }else{
                success=1;
            }
        }else{
                nextsubstart=srtToSeconds($('tr#tr_'+2+' .subStart').html());
                if(stimesec>etimesec){
                    swal('Start time overlaps end time by '+(stimesec-etimesec).fixed(3)+' seconds');
                }else if(etimesec>nextsubstart){
                    swal('End time overlaps next subtitle by '+(etimesec-nextsubstart).fixed(3)+' seconds');
                }else{
                    success=1;
                }
            }
    }else{
            if(id!=$('#subtitles-table tbody tr').length) {
                id -= 1;
                prevsubend = srtToSeconds($('tr#tr_' + id + ' .subEnd').html());
                id += 2;
                nextsubstart = srtToSeconds($('tr#tr_' + id + ' .subStart').html());
                if (stimesec > etimesec) {
                    swal('Start time overlaps end time by ' + (stimesec - etimesec).toFixed(3) + ' seconds');
                } else if (etimesec > nextsubstart) {
                    swal('End time overlaps next subtitle by ' + (etimesec - nextsubstart).toFixed(3) + ' seconds');
                } else if (stimesec < prevsubend) {
                    swal('Previous subtitle overlaps start time by ' + (prevsubend - stimesec).toFixed(3) + ' seconds');
                } else {
                    success = 1;
                }
            }else{
                id -= 1;
                prevsubend = srtToSeconds($('tr#tr_' + id + ' .subEnd').html());
                if (stimesec > etimesec) {
                    swal('Start time overlaps end time by ' + (stimesec - etimesec).toFixed(3) + ' seconds');
                }else if (stimesec < prevsubend) {
                    swal('Previous subtitle overlaps start time by ' + (prevsubend - stimesec).toFixed(3) + ' seconds');
                } else {
                    success = 1;
                }
            }
    }
    if(success){
        var id=key;
        var start_time = $('#start-time-input').val();
        var end_time = $('#end-time-input').val();
        var content = $('#content-input').val();
        content=content.replace('\n','\\n');
        var jsonaddsub = $.parseJSON('{"starttime":"' + start_time + '","endtime":"' + end_time + '","content":"' + content + '"}');
        var jsontosent=[];
        toJsonArray(jsontosent);
        $.ajax({
            url:"http://localhost:8080/OnlineSubtitleEditor/rest/subtitleService/insertSubtitles/",
            method:'POST',
            contentType:"application/json; charset=utf-8",
            dataType:"text",
            data:JSON.stringify(jsontosent),
            success:function(){
                $.ajax({
                    url:"http://localhost:8080/OnlineSubtitleEditor/rest/subtitleService/subtitle/"+id,
                    method:'PUT',
                    data: JSON.stringify(jsonaddsub),
                    contentType: "application/json",
                    dataType: "text",
                    success:function(){
                        $('tr.selected .subStart').html($('#start-time-input').val());
                        $('tr.selected .subEnd').html($('#end-time-input').val());
                        $('tr.selected .subContent>span').html($('#content-input').val());
                        updateTable();
                    }
                });
            }
        });
    }


}
/**
 * This function is responsible for adding the subtitle after a selected subtitle
 * from the table. This function contains validations about the correctness of the time inputs
 * based on the time inputs of its nearest subtitles.It also has ajax calls for adding the
 * subtitle to the back end
 */
function insertAfterSelectedSub(){
    var key = $('#subtitles-table tbody tr.selected').attr('id').split('_')[1];
    var id=parseInt($('tr.selected').attr('id').split('_')[1]);
    var stimesec=srtToSeconds($('#start-time-input').val());
    var etimesec=srtToSeconds($('#end-time-input').val());
    var nextsubstart;
    var prevsubend=srtToSeconds($('tr#tr_' + key + ' .subEnd').html());
    var success=0;
    if(key==1){
        id=1;
    }else{
        id=key;
    }

    if(id==1){
        if($('tr#tr_'+2+' .subStart').html()==undefined){
            if(stimesec>etimesec){
                swal('Start time overlaps end time by '+(stimesec-etimesec)+' seconds');
            }else{
                success=1;
            }
        }else{
            nextsubstart=srtToSeconds($('tr#tr_'+2+' .subStart').html());
            if(stimesec>etimesec){
                swal('Start time overlaps end time by '+(stimesec-etimesec)+' seconds');
            }else if(etimesec>nextsubstart){
                swal('End time overlaps next subtitle by '+(etimesec-nextsubstart)+' seconds');
            }else{
                success=1;
            }
        }
    }else {
        if(id!=$('#subtitles-table tbody tr').length) {
            id ++;
            nextsubstart = srtToSeconds($('tr#tr_' + id + ' .subStart').html());
            if (stimesec > etimesec) {
                swal('Start time overlaps end time by ' + (stimesec - etimesec).toFixed(3) + ' seconds');
            } else if (etimesec > nextsubstart) {
                swal('End time overlaps next subtitle by ' + (etimesec - nextsubstart).toFixed(3) + ' seconds');
            } else if (stimesec < prevsubend) {
                swal('Previous subtitle overlaps start time by ' + (prevsubend - stimesec).toFixed(3) + ' seconds');
            } else {
                success = 1;
            }
        }else{
            if (stimesec > etimesec) {
                swal('Start time overlaps end time by ' + (stimesec - etimesec).toFixed(3) + ' seconds');
            }else if (stimesec < prevsubend) {
                swal('Previous subtitle overlaps start time by ' + (prevsubend - stimesec).toFixed(3) + ' seconds');
            } else {
                success = 1;
            }
        }
    }
    if(success){
        key++;
        var start_time = $('#start-time-input').val();
        var end_time = $('#end-time-input').val();
        var content = $('#content-input').val();
        content=content.replace('\n','\\n');
        var jsonaddsub = $.parseJSON('{"starttime":"' + start_time + '","endtime":"' + end_time + '","content":"' + content + '"}');
        var jsontosent=[];
        toJsonArray(jsontosent);
        $.ajax({
            url:"http://localhost:8080/OnlineSubtitleEditor/rest/subtitleService/insertSubtitles/",
            method:'POST',
            contentType:"application/json; charset=utf-8",
            dataType:"text",
            data:JSON.stringify(jsontosent),
            success:function(){
                $.ajax({
                    url: "http://localhost:8080/OnlineSubtitleEditor/rest/subtitleService/subtitle/" + key,
                    method: 'POST',
                    data: JSON.stringify(jsonaddsub),
                    contentType: "application/json",
                    dataType: "text",
                    success: function () {
                        $('#subtitles-table tbody #tr_' + (key - 1)).after('<tr id="tr_' + key + '" role="row"></tr>');

                        $('#tr_' + key).append('<td class="subID">' + -1 + '</td>');
                        $('#tr_' + key).append('<td class="subStart">' + $('#start-time-input').val() + '</td>');
                        $('#tr_' + key).append('<td class="subEnd">' + $('#end-time-input').val() + '</td>');
                        $('#tr_' + key).append('<td class="subContent"><span>' + $('#content-input').val() +
                        '</span><button type="button" class="delete_row close" aria-label="Close">' +
                        '<span aria-hidden="true">&times;</span></button></td>');
                        updateTable();
                        $('#subtitles-table tr.selected').children('.subContent').children('.delete_row').css('visibility', 'hidden');
                        $('#subtitles-table tr.selected').removeClass('selected');
                        $('body #subtitles-table #tr_' + key).children('.subContent').children('.delete_row').css('visibility', 'visible');
                        $('body #subtitles-table #tr_' + key).addClass('selected');
                        $('#start-time-input').val($('tr.selected .subStart').html());
                        $('#end-time-input').val($('tr.selected .subEnd').html());
                        $('#content-input').val($('tr.selected .subContent span').html());
                        $('#confirm-wrapper .errormsg').remove();
                    }
                });
            }
        });
    }

}
/**
 * This function is responsible for adding the subtitle before a selected subtitle
 * from the table. This function contains validations about the correctness of the time inputs
 * based on the time inputs of its nearest subtitles.It also has ajax calls for adding the
 * subtitle to the back end
 */
function insertBeforeSelectedSub(){
    var key = $('#subtitles-table tbody tr.selected').attr('id').split('_')[1];
    var id=parseInt($('tr.selected').attr('id').split('_')[1]);
    var stimesec=srtToSeconds($('#start-time-input').val());
    var etimesec=srtToSeconds($('#end-time-input').val());
    var nextsubstart=srtToSeconds($('tr#tr_' + key + ' .subStart').html());;
    var prevsubend;
    var success=0;
    if(key==1){
        id=1;
    }else{
        id=key;
    }

    if(id==1){
        nextsubstart=srtToSeconds($('tr#tr_'+1+' .subStart').html());
        if (stimesec > etimesec) {
            swal('Start time overlaps end time by ' + (stimesec - etimesec).toFixed(3) + ' seconds');
        }else if(etimesec>nextsubstart){
            swal('End time overlaps next subtitle\'s start time by '+(etimesec-nextsubstart).toFixed(3)+' seconds');
        }else {
            success = 1;
        }
    }else {
        id--;
        prevsubend=srtToSeconds($('tr#tr_' + id + ' .subEnd').html());
        if(stimesec>etimesec){
            swal('Start time overlaps end time by '+(stimesec-etimesec).toFixed(3)+' seconds');
        }else if (stimesec < prevsubend) {
            swal('Previous subtitle overlaps start time by ' + (prevsubend - stimesec).toFixed(3) + ' seconds');
        }else if(etimesec>nextsubstart){
            swal('End time overlaps next subtitle\'s start time by '+(etimesec-nextsubstart).toFixed(3)+' seconds');
        }else{
            $('#subtitles-table').append('<tbody></tbody>');
            success=1;
        }
    }
    if(success) {
        var start_time = $('#start-time-input').val();
        var end_time = $('#end-time-input').val();
        var content = $('#content-input').val();
        content=content.replace('\n','\\n');
        var jsonaddsub = $.parseJSON('{"starttime":"' + start_time + '","endtime":"' + end_time + '","content":"' + content + '"}');
        success=0;
        var jsontosent=[];
        toJsonArray(jsontosent);
        $.ajax({
            url:"http://localhost:8080/OnlineSubtitleEditor/rest/subtitleService/insertSubtitles/",
            method:'POST',
            contentType:"application/json; charset=utf-8",
            dataType:"text",
            data:JSON.stringify(jsontosent),
            success:function(){
                $.ajax({
                    url:"http://localhost:8080/OnlineSubtitleEditor/rest/subtitleService/subtitle/"+key,
                    method:'POST',
                    data:JSON.stringify(jsonaddsub),
                    contentType:"application/json; charset=utf-8",
                    dataType:"text",
                    success:function(){
                        $('#subtitles-table tbody #tr_' + key).before('<tr class="odd" id="tr_' + -1 + '" role="row"></tr>');
                        $('#tr_' + -1).append('<td class="subID">' + -1 + '</td>');
                        $('#tr_' + -1).append('<td class="subStart">' + $('#start-time-input').val() + '</td>');
                        $('#tr_' + -1).append('<td class="subEnd">' + $('#end-time-input').val() + '</td>');
                        $('#tr_' + -1).append('<td class="subContent"><span>' + $('#content-input').val() +
                        '</span><button type="button" class="delete_row close" aria-label="Close">' +
                        '<span aria-hidden="true">&times;</span></button></td>');
                        updateTable();
                        $('#subtitles-table tr.selected').children('.subContent').children('.delete_row').css('visibility', 'hidden');
                        $('#subtitles-table tr.selected').removeClass('selected');
                        $('#subtitles-table #tr_' + key).children('.subContent').children('.delete_row').css('visibility', 'visible');
                        $('#subtitles-table #tr_' + key).addClass('selected');
                        $('#start-time-input').val($('tr.selected .subStart').html());
                        $('#end-time-input').val($('tr.selected .subEnd').html());
                        $('#content-input').val($('tr.selected .subContent span').html());
                        $('#confirm-wrapper .errormsg').remove();
                    }
                });
            }
        });
    }
}
/**
 * This function updates the subtitles table and the subtitles that are displayed on the video.
  */
function updateTable(){
    Popcorn.destroy(example_video);
    example_video = Popcorn( "#video-container" );
    $('#subtitles-table tbody tr').each(function (key,value) {
        $(this).removeAttr('id');
        $(this).attr("id","tr_"+(key+1));
        $(this).children('.subID').html(key+1);
        if($(this).attr("id").split('_')[1]%2==0) {
            $(this).removeClass('odd');
            $(this).addClass('even');
        }else{
            $(this).removeClass('even');
            $(this).addClass('odd');
        }
        example_video.subtitle({
            start: srtToSeconds($('#tr_'+(key+1)+' .subStart').html()),
            end: srtToSeconds($('#tr_'+(key+1)+' .subEnd').html()),
            target:"subtitle",
            text: '<span>'+$('#tr_'+(key+1)+' .subContent span').html()+'</span>'
        });
    } );
}

/**
 *
 * This function creates an array of the subtitles that are in the table.
 * The format is similar to a json array because it will be used for it sending in JSON format
 *
 * @param jsonarray an array of subtitles
 */
function toJsonArray(jsonarray){
    var startime;
    var endtime;
    var content;
    var subtitlejson;
    $('#subtitles-table tbody tr').each(function (key1,value1) {
        startime=$(this).children('.subStart').html();
        endtime=$(this).children('.subEnd').html();
        content=$(this).children('.subContent').children('span').html();
        subtitlejson={"starttime":startime,"endtime":endtime,"content":content};
        jsonarray.push(subtitlejson);
    });
}

