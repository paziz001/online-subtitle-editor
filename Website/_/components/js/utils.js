function secondsToSrtTime(seconds) {
    var hours = new String(parseInt(seconds / 60 / 60));
    var minutes = new String(parseInt(seconds / 60 % 60));
    var s = new String(parseInt(seconds % 60));
    var milliseconds = new String(parseInt(seconds % 1 * 1000));
    if (hours.length == 1)
        hours = "0" + hours;
    if (minutes.length == 1)
        minutes = "0" + minutes;
    if (s.length == 1)
        s = "0" + s;
    while (milliseconds.length < 3)
        milliseconds = "0" + milliseconds;
    return new String(hours + ":" + minutes + ":" + s + "." + milliseconds);
}
function millisecondsToSrtTime(milliseconds) {
    var hours = new String(parseInt(milliseconds / 60 / 60 / 1000));
    var minutes = new String(parseInt(milliseconds / 60 / 1000 % 60));
    var s = new String(parseInt(milliseconds / 1000 % 60));
    var milliseconds = new String(parseInt(milliseconds % 1000));
    if (hours.length == 1)
        hours = "0" + hours;
    if (minutes.length == 1)
        minutes = "0" + minutes;
    if (s.length == 1)
        s = "0" + s;
    while (milliseconds.length < 3)
        milliseconds = "0" + milliseconds;
    return new String(hours + ":" + minutes + ":" + s + "." + milliseconds);
}
function srtToSeconds(srt) {
    var arr = srt.split(":");
    var hours = arr[0];
    var minutes = arr[1];
    var seconds = arr[2];
    return parseFloat(hours) * 60 * 60 + parseFloat(minutes) * 60 + parseFloat(seconds);
}
function srtToMilliseconds(srt) {
    var arr = srt.split(":");
    var hours = arr[0];
    var minutes = arr[1];
    var seconds = arr[2].split('.')[0];
    var milliseconds = arr[2].split('.')[1];
    return parseFloat(hours) * 60 * 60 * 1000 + parseFloat(minutes) * 60 * 1000 + parseFloat(seconds) * 1000 + parseFloat(milliseconds);
}